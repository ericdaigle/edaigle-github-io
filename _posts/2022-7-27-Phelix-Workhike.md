---
layout: post
title: Phelix Hut Workhike
---

Someone on the VOC huts page reported back in June that the roof at Phelix was "falling apart." Roland [went up](https://www.ubc-voc.com/2022/07/04/phelix-hut-july-1-3) a few weeks later to investigate further and came back with a few more details: the wood that the sheet metal was nailed into at the edge of the roof had fallen off and needed to be re-attached. Looked like the floor needed a fresh coat of paint as well. Last weekend seemed like a nice and sunny opportunity to get this done, so we loaded up the Neurotic Jeep with the requisite tools and left Vancouver at 4pm. 

After picking up John and a slightly drugged (owing to a dentist appointment earlier in the day) Haley and making the requisite stop at the Pemberton McDonald's, we made it to the Phelix Creek FSR turnoff around 8. A landslide about a kilometer down the road was complicating access - since we arrived well before the other two car groups, we decided to spend some time digging it out, and after an hour or so had moved enough dirt and rock to be able to cross relatively comfortably in a 4WDHC.

<figure>
  <img src="https://user-images.githubusercontent.com/8319652/181172014-98d69251-c9fe-4eed-bf88-ea300fe6b20f.png">
  <figcaption align = "center">
    <p><i>Roland, John, and Alex hard at work clearing the road. Apologies to our ice axes. Photo: Eric Daigle</i></p>
  </figcaption>
</figure>

Satisfied with our work, we headed back down to the turnoff and set up camp, then enjoyed dinner and some stargazing before heading to sleep. We got off to a 6am start the next morning, cooked breakfast, and began the car shuttling process up Phelix Creek FSR to the Lego Blocks. By 8am we were on the trail. Aside from a wobbly creek crossing where Ketan's camera went for a swim, the hike up was calm and pleasant (who knew you could get to a hut without spending 8-9 hours skinning and cursing your life decisions?). The first of us to get up made it just before 11, and the rest of the group followed shortly after. The snow melted very quickly since Roland was last up - there's essentially nothing left around the hut, although the ground is a bit damp. The lake has completely thawed as well, and water level is quite high - if you follow the trail you'll likely get wet even walking across the rocks unless your boots are very tall. We took a quick lunch and a very <i>refreshing</i> swim before getting to work.

<figure>
  <img src="https://user-images.githubusercontent.com/8319652/181175097-12c1a286-089b-458a-8987-f342e6a35f45.png">
  <figcaption align = "center">
    <p><i>Snow? What snow? Photo: Ketan Desai</i></p>
  </figcaption>
</figure>

A quick look at the roof made it pretty obvious the plywood on the inside is rotting. Rebuilding the roof is definitely not a project for this year, though, so we settled for a band-aid fix - Alex cut off the ends of the crossbeams that were sticking out and nailed the tin back down directly. Nothing is at risk of coming down on someone's head now. We decided the floors didn't actually need painting after all, but the porch was pretty obviously sagging - we rebuilt it with newly painted beams.

<figure>
  <img src="https://user-images.githubusercontent.com/8319652/181182525-a38d78b8-8a3c-460d-9bc8-2951dfbab506.png">
  <figcaption align = "center">
    <p><i>Martin, Ketan, and Anton working on the porch. Photo: Ketan Desai</i></p>
  </figcaption>
</figure>

With the work out of the way, the original plan for the next day was for some of us to attempt a scramble route up Gandalf while the others relaxed at the hut. Unfortunately a bit of exploration by Anton and Ketan revealed that the boulder fields along the route were still covered in just enough snow to be able to punch through and break an ankle, so the objective shifted to the seemingly drier Shadowfax and Anton, Aye Min, Martin, Emily, Haley and I set off at 5:30 Sunday morning. After some steep bushwhacking and a surprisingly sketchy traverse to the north, we gained the western plateau approaching the summit. From here what we believed to be the usual route was still covered in snow, and several alternatives we explored (thanks Martin and Emily for your bravery) proved to be exposed beyond our comfort levels. With nobody particularly in the mood to start a snow climb, we contented ourselves with excellent views from the plateau before descending, reconvening with the others around noon, and heading back to the cars.

<figure>
  <img src="https://user-images.githubusercontent.com/8319652/181181354-d37aaa13-9b5d-4363-8410-c5985f9f5289.png">
  <figcaption align = "center">
    <p><i>Not a bad view from the turnaround spot. Photo: Eric Daigle</i></p>
  </figcaption>
</figure>

A few housekeeping notes:
* Road is fine all the way to the Lego Blocks with 4WDHC (even if slide area is slightly nerve-wracking)
* Cashbox was emptied, money is with Roland
* Hut is running a bit low on firewood. Ketan split some of what was there. Will need to be replenished in the fall
