---
layout: page
title: About
permalink: /about/
---

Hey, I'm Eric, a fourth-year student in computer science and economics at the University of British Columbia in Vancouver, Canada. My main interests are in applying data science and geomatics tools to solve problems in sustainability and urban planning. I'm also a systems programmer and free software advocate, regularly contributing to several open source projects.

I currently work at [Xtract AI](https://xtract.ai/) as a geomatics software engineering intern, where I am using Python and C to add ArcGIS integration to a proprietary machine learning pipeline. To see some other things I've worked on, take a look at the links in the header. 

In my free time I enjoy hiking, skiing, and mountaineering in the beautiful Lower Mainland with UBC's [Varsity Outdoor Club](https://www.ubc-voc.com/).

### Contact me

[eric.daigle@posteo.net](mailto:eric.daigle@posteo.net)

### Licence

All content on this site is released under the <a rel="license"
href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC A-NC-SA 4.0 International Licence</a>.
