---
layout: page
title: Free Software Involvement
permalink: /free/
---
- [GNOME](https://gitlab.gnome.org/edaigle)  
I contribute bug fixes and new features across the GNOME stack including Shell, Nautilus, and Settings. I mostly work in C with some contributions in JavaScript, Vala, and Python. 
