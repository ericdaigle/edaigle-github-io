---
layout: page
title: Projects
permalink: /projects/
---

#### Academic

- [High School Student Drug Use Analysis](https://raw.githubusercontent.com/edaigle/edaigle.github.io/master/econ398.pdf)  
Used R and the tidyverse data science suite (code available upon request) to analyze Statistics Canada's _Student Alcohol, Drug, and Tobacco Use Survey_ and investigate whether patterns of student substance abuse differ between rural and urban high schools. Evaluated causality of my results using several econometric models, and presented my conclusions in a written report. For ECON 398: Introduction to Economic Research, with Prof. Jonathan Graves.

- [Vancouver Neighbourhood Sustainability Analysis](https://raw.githubusercontent.com/edaigle/edaigle.github.io/master/des230.pdf)  
Analyzed Statistics Canada datasets using QGIS and Python (project and code available upon request) to examine sustainability performance across eight Vancouver neighbourhoods, defining and comparing metrics for density, affordability, and sustainable mobility. Used results to produce a report identifying existing strengths and opportunities for improvement. For DES 230: Sustainability by Design, with Prof. Patrick Condon.

#### Personal 

- [GNES](https://github.com/edaigle/GNES)  
A work-in-progress NES emulator for Linux written from the ground up in C with a native GTK4 GUI frontend, using SDL for video and audio output.

- [Drive or Fly](https://github.com/edaigle/drive-or-fly)  
A simple web application using JavaScript and React to calculate whether a trip should be made by car or by plane to minimize carbon footprint. Fuel economy and routing info sourced from the FuelEconomy.gov, Bing Maps, and GoClimate APIs.

- [Stocks for Linux](https://github.com/edaigle/stocks-for-linux)  
A stock price tracker for GNOME written in C and GTK4, inspired by Apple Stocks for macOS. Market data sourced from the Questrade API. 

